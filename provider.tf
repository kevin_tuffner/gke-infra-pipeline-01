provider "google" {
  credentials = file("service_account.json")
  project     = "gke-terraform-1"
  region      = "us-central1"
}
