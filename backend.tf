terraform {
  backend "gcs"{
    bucket      = "tfstate01"
    prefix      = "dev"
    credentials = "service_account.json"
  }
}
